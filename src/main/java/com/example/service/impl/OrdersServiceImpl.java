package com.example.service.impl;

import com.example.domain.Orders;
import com.example.domain.OrdersCustom;
import com.example.domain.User;
import com.example.mapper.OrdersMapper;
import com.example.mapper.UserMapper;
import com.example.service.OrdersService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersMapper ordersMapper;



    @Override
    public List<User> findByPage(Integer pageNow, Integer rows) {
        int start = (pageNow-1) * rows;
        return ordersMapper.findByPage(start,rows);
    }

    @Override
    public Long findTotals() {
        return ordersMapper.findTotals();
    }

    //使用AR完成CRUD
    @Override
    public void delete(Integer id) {
        User user = new User();
        user.setId(id);
        user.deleteById();

    }

    //延迟加载
    @Override
    public List<Orders> findOrdersUserLazyLoading() {
        return ordersMapper.findOrdersUserLazyLoading();
    }

    //一对多查询
    @Override
    public List<User> findUserAndItemsResultMap() {
        return ordersMapper.findUserAndItemsResultMap();
    }


    //一对多查询
    @Override
    public List<Orders> findOrdersAndOrderDetailResultMap() {
        return ordersMapper.findOrdersAndOrderDetailResultMap();
    }


    //一对一查询(resultMap，可实现延迟加载)
    @Override
    public List<Orders> findOrdersUserResultMap() {
        return ordersMapper.findOrdersUserResultMap();
    }

    //一对一查询(resultType)
    @Override
    public List<OrdersCustom> findOrdersUser() {
        return ordersMapper.findOrdersUser();
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)   //使用事务
    public List<Orders> findAll() {
        return ordersMapper.findAll();
    }

    @Override
    public Orders findById(Integer id) {
        return ordersMapper.findById(id);
    }

    @Override
    public void save(Orders orders) {
        ordersMapper.save(orders);
    }

    @Override
    public void update(Orders orders) {
        ordersMapper.update(orders);
    }



    @Override
    public List<User> findAlls() {
        User user = new User();
        List<User> users = user.selectList(null);
        return users;
    }
}
