package com.example.service;

import com.example.domain.Orders;
import com.example.domain.OrdersCustom;
import com.example.domain.User;

import java.util.List;

public interface OrdersService {

    //分页查询
    List<User> findByPage(Integer pageNow,Integer rows);

    //查询总条数
    Long findTotals();

    //使用AR插件删除
    void delete (Integer id);

    //延迟加载
    List<Orders> findOrdersUserLazyLoading();

    //多对多查询(resultMap)
    List<User> findUserAndItemsResultMap();


    //一对多查询(resultMap)
    List<Orders> findOrdersAndOrderDetailResultMap();

    //一对一查询(resultMap)
    List<Orders> findOrdersUserResultMap();

    //一对一查询(resultType)
    List<OrdersCustom> findOrdersUser();

    //CRUD
    List<Orders> findAll();

    Orders findById(Integer id);

    void save(Orders computers);

    void update(Orders computers);

    // void delete(Integer id);

	List<User> findAlls();
}
