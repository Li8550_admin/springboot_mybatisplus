package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.domain.Orders;
import com.example.domain.OrdersCustom;
import com.example.domain.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface OrdersMapper extends BaseMapper<User> {

    // 分页查询
    List<User> findByPage(@Param("start") Integer start,@Param("rows") Integer rows);

    //查询总条数
    Long findTotals();

    //查询订单关联用户，用户信息需要延迟加载
    List<Orders> findOrdersUserLazyLoading();

    //多对多查询（resultMap)
    List<User> findUserAndItemsResultMap();


    //一对多查询（resultMap)
    List<Orders> findOrdersAndOrderDetailResultMap();

    //一对一查询（resultMap,可实现延迟加载)
    List<Orders> findOrdersUserResultMap();

    //一对一查询（resultType)
    List<OrdersCustom> findOrdersUser();

    //简单CRUD
    List<Orders> findAll();

    Orders findById(Integer id);

    void save(Orders user);

    void update(Orders user);

    void delete(Integer id);

    int deleteByPrimaryKey(Integer id);

    int insert(Orders record);

    int insertSelective(Orders record);

    Orders selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Orders record);

    int updateByPrimaryKey(Orders record);


}