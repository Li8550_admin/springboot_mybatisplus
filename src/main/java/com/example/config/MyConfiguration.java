package com.example.config;

import com.baomidou.mybatisplus.core.parser.ISqlParser;
import com.baomidou.mybatisplus.extension.parsers.BlockAttackSqlParser;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.extension.plugins.SqlExplainInterceptor;
import com.example.plugins.MyInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration//配置类
public class MyConfiguration {

    @Bean//把当前方法返回的对象注入到Spring容器中，<bean id class>
    public RestTemplate restTemplate(){
        return  new RestTemplate();
    }


    // @Bean  //注入自己的拦截器
    // public MyInterceptor myInterceptor() {
    //     return new MyInterceptor();
    // }


    @Bean     //执行分析插件（可阻断全表更新删除）
    public SqlExplainInterceptor sqlExplainInterceptor () {
        SqlExplainInterceptor sqlExplainInterceptor = new SqlExplainInterceptor();

        List<ISqlParser> list = new ArrayList<>();
        list.add(new BlockAttackSqlParser());    //全表更新、删除的阻断器

        sqlExplainInterceptor.setSqlParserList(list);

        return sqlExplainInterceptor;
    }

    @Bean    //性能分析插件
    public PerformanceInterceptor performanceInterceptor () {
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        performanceInterceptor.setMaxTime(100);
        performanceInterceptor.setFormat(true);
        return performanceInterceptor;
    }

}

