package com.example.User.service;

import com.example.User.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-01
 */
public interface IUserService extends IService<User> {

}
