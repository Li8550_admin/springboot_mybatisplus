package com.example.User.mapper;

import com.example.User.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-01
 */
public interface UserMapper extends BaseMapper<User> {

}
