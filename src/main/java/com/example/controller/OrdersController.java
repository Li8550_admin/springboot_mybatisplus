package com.example.controller;

import com.baomidou.mybatisplus.extension.api.R;
import com.example.domain.Orders;
import com.example.domain.OrdersCustom;
import com.example.domain.User;
import com.example.service.OrdersService;
import com.example.vo.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin    //跨域问题
@RestController
@RequestMapping("/user")
public class OrdersController {

    @Autowired
    OrdersService ordersService;


    // 分页查询
    @GetMapping("/findByPage")
    public Map<String ,Object> findByPage(Integer pageNow, Integer pageSize){
        Map<String,Object> result = new HashMap<>();
        pageNow = pageNow==null?1:pageNow;
        pageSize = pageSize==null?4:pageSize;
        List<User> users = ordersService.findByPage(pageNow, pageSize);
        Long totals = ordersService.findTotals();
        result.put("users",users);
        result.put("total",totals);
        return result;
    }

    @GetMapping("/findAlls")
    public List<User> findAlls() throws JsonProcessingException {
        return ordersService.findAlls();
    }

    @GetMapping("/deletes")
    public Result delete(Integer id)  {
        Result result = new Result();
        try {
            ordersService.delete(id);
            result.setMsg("删除用户信息成功！");
        }catch (Exception e){
            e.printStackTrace();
            result.setStatus(false);
            result.setMsg("删除用户信息失败，请稍后再试！");
        }
        return result;
    }


    @PostMapping("/saveuser")
    public Result save(@RequestBody User user) {
        Result result = new Result();
        try {
            User user1=user;
            user1.insert();
            result.setMsg("用户信息保存成功！");
        }catch (Exception e){
            result.setStatus(false);
            result.setMsg("系统错误：保存用户信息失败，请稍后再试...");

        }
        return result;
    }



    //延迟加载
    @RequestMapping("/findOrdersUserLazyLoading")
    public List<Orders> findOrdersUserLazyLoading() throws JsonProcessingException {
        return ordersService.findOrdersUserLazyLoading();
    }

    //多对多查询
    @RequestMapping("/findUserAndItemsResultMap")
    public List<User> findUserAndItemsResultMap() throws JsonProcessingException {
        return ordersService.findUserAndItemsResultMap();
    }

    //一对多查询
    @RequestMapping("/findOrdersAndOrderDetailResultMap")
    public List<Orders> findOrdersAndOrderDetailResultMap() throws JsonProcessingException {
        return ordersService.findOrdersAndOrderDetailResultMap();
    }

    //一对一查询(resultMap，可实现延迟加载)
    @RequestMapping("/findOrdersUserResultMap")
    public List<Orders> findOrdersUserResultMap() throws JsonProcessingException {
        return ordersService.findOrdersUserResultMap();
    }

    //一对一查询(resultType)
    @RequestMapping("/findOrdersUser")
    public List<OrdersCustom> findOrdersUser() throws JsonProcessingException {
        return ordersService.findOrdersUser();
    }

    //CRUD
    //     @RequestMapping("/findAll")
    //     public List<Orders> findAll() throws JsonProcessingException {
    //          return ordersService.findAll();
    //     }

    //根据id查询
    @RequestMapping("/findById/{id}")
    public Orders findById(@PathVariable("id") Integer id) {
        return ordersService.findById(id);
    }

    //新增
    @RequestMapping("/save")
    public void save(Orders computers) {
        ordersService.save(computers);
    }

    //修改
    @PutMapping("/update/{id}")
    public void update(@PathVariable("id") Integer id,Orders computers) {
        ordersService.update(computers);
    }

    //删除
    // @RequestMapping("/delete/{id}")
    // public void delete(@PathVariable("id") Integer id) {
    //     ordersService.delete(id);
    // }
}
