package com.example.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.domain.Orders;
import com.example.domain.User;
import com.mysql.cj.xdevapi.Column;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrdersMapperTest {

    @Autowired
    private OrdersMapper ordersMapper;



    @Test
    public void testARSelect() {
        User user = new User();
        List<User> users = user.selectList(null);
        //iter
        for (User user1 : users) {
            System.out.println(user1);
        }
    }

    @Test
    public void testAR() {
        User user = new User();
       user.setId(3);
        user.setUsername("小乔");
        boolean user2 = user.updateById();
        System.out.println(user2);
    }

    @Test
    public void testSelectList(){
        User user = new User();
        user.setId(3);
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
//        User user1 = ordersMapper.selectOne(wrapper);
//        wrapper.gt("id",user.getId());  大于
//         Integer integer = ordersMapper.selectCount(wrapper);

        // System.out.println(integer);

    }

    //延迟加载
    @Test
    public void findOrdersUserLazyLoading() {
        // List<Orders> list = ordersMapper.findOrdersUserLazyLoading();
        // System.out.println(list);
        // for (Orders orders:list) {
        //     User user = orders.getUser();
        //     System.out.println(user);
        // }
    }
}